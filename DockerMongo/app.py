import os
from pymongo import MongoClient
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow
import acp_times
import config
import logging

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

item_list = []
number = 1

@app.route('/')
@app.route('/index')
def todo():
    app.logger.debug('Main page entry')
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html', items=items)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug('Page not found')
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route('/_calc_times')
def _calc_times():
    app.logger.debug('Got a JSON request')

    km = request.args.get('km', 999, type=float)
    mile = request.args.get('mile', type=float)
    brevet_dist = int(request.args.get("brevet_dist", type=str))
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)
    date_time = arrow.get(begin_date + ' ' + begin_time, 'YYYY-MM-DD HH:mm')

    app.logger.debug('km={}'.format(km))
    app.logger.debug('request.args: {}'.format(request.args))
    
    open_time = acp_times.open_time(km, brevet_dist, date_time)
    close_time = acp_times.close_time(km, brevet_dist, date_time)
    result = {'open': open_time, 'close': close_time}

    global number
    item_doc = {
        'number': number,
        'km': km,
        'mile': mile,
        'distance': brevet_dist,
        'begin_date': begin_date,
        'begin_time': begin_time,
        'open_time': arrow.get(open_time).format('YYYY-MM-DD HH:mm'),
        'close_time': arrow.get(close_time).format('YYYY-MM-DD HH:mm'),
    }
    item_list.append(item_doc)
    number += 1
        
    return flask.jsonify(result=result)

@app.route('/new', methods=['POST'])
def new():
    if len(item_list) == 0:
        return redirect(url_for('wrong'))
    
    db.tododb.delete_many({})
    db.tododb.insert_many(item_list)
    
    if item_list[-1]['km'] < item_list[-1]['distance']:
        del item_list[:]
        number = 1
        return redirect(url_for('wrong'))
    

    del item_list[:]
    number = 1

    return redirect(url_for('todo'))

@app.route('/wrong')
def wrong():
    _items = db.tododb.find()
    items = [item for item in _items]
    db.tododb.delete_many({})

    return render_template('wrong.html', items=items[-1])

@app.route('/display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('display.html', items=items)


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print('Opening for global access on port {}'.format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host='0.0.0.0', debug=True)
