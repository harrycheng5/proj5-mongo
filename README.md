Author: Harry Cheng

Repo: https://bitbucket.org/harrycheng5/proj5-mongo/

Description:  It is a web app that can compute the open time and close time for brevet ride based on the control points user has put in. For the rule of brevet, you can check here (https://rusa.org/pages/rulesForRiders).
# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## What is in this repository

app.py has the funtionalities of server, including flask, AJAX, and mongodb, whereas the client side is located in html files in templates folder.

## What does it do

It is improved version of web app that is based upon project 4. You can find the basic information here (https://bitbucket.org/harrycheng5/proj4-brevets/). 

This program computes the open time and close time for brevet ride. Users simply need to enter the length of brevet ride and the control points with other needed information. Then it will calculate them automatically for you.

For developers, the knowledges behind this project are AJAX and Flask. AJAX is responsible for the client side where it controls what users see on the website. It also sends the needed data back and forth between the client and server and updates the information on the website at the same time. Flask is implemented in the server side where all our algorithms are computed there. The rule of our algorithms is based on here (https://rusa.org/pages/acp-brevet-control-times-calculator).
The main algorithm used in this project is in the two functions in acp_times.py open_time(control_dist_km, brevet_dist_km, brevet_start_time) close_time(control_dist_km, brevet_dist_km, brevet_start_time)
## Functionality I have added 

Besides the automated calculations of open and close time that are displayed immediately on web screen, it now contains 2 button: submit and display.
When submit button is clicked, it will send the data of each control point to the client side (app.py), including open and closetime, starting date, and distance, etc. The client side will then send it to mongodb database where all the information is stored.
After that, you can click display button where you will be directed to another web page to display all the information you just have entered. 
Remember, if you click display button before clicking submit button, you won't see anything because the information has not been sent to database.

## To use this program

clone this repo and pass it to your terminal. Change your directory to proj5-mongo and type $ docker-compose up, docker-compose.yml will build essential containers for you and launch the web app.
You cna then start using this program by enter http://localhost:5000/ in your browser.

## Test Cases for submit and display button

### example 1: Enter all the required information (the last control point is longer or equal to brevet distance)
open and close time should be calculated and showed automatically on calc.html web page.
When you then click submit button, the data is sent and stored in database, and web page will be refreshed with empty form again.
Clicking display button now will lead you to display.html web page showing the information you just stored in database.

Also, the database will only store information for one brevet ride. In other words, when you click display button, you can only see the information about the last brevet ride you submitted, even if you entered and submitted two different brevet rides continuously. 

### example 2: no control times is entered
If you click submit button now, you will be led to wrong.html web page telling you that you didn't enter your control times properly.
Clicking display button will result in a empty web page because the database does not store anything yet.

### example 3: the last control point is shorter than brevet distance
It will lead to the same result of example 3 because you didn't use the web app as expected.

### example 4: display before submit
As mentioned before, it will show a web page with nothing in there because you have not entered data through submit button yet. However, if you do this, it will not cause error though.
